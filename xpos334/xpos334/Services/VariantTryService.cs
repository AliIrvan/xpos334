﻿using AutoMapper;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class VariantTryService
    {
        private readonly XPOS_334Context db;
        VMResponse respon = new VMResponse();
        int IdUser = 1;

        public VariantTryService(XPOS_334Context _db)
        {
            db = _db;
        }
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariant, VMTblVariant>();
                cfg.CreateMap<VMTblVariant, TblVariant>();
            });

            IMapper mapper = config.CreateMapper();
            return mapper;
        }

        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> dataView = ( from v in db.TblVariants
                                            join c in db.TblCategories on v.IdCategory equals c.Id
                                            where v.IsDelete == false
                                            select new VMTblVariant
                                            {
                                                Id = v.Id,
                                                NameVariant = v.NameVariant,
                                                Description = v.Description,
                                                NameCategory = c.NameCategory,
                                                IdCategory = v.IdCategory,
                                            }).ToList();
            return dataView;
        }

        public VMResponse Create(VMTblVariant dataView)
        {
            //TblVariant dataModel = GetMapper().Map<TblVariant>(dataView);
            TblVariant dataModel = new TblVariant();
            dataModel.NameVariant= dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.IsDelete = false;
            dataModel.Description = dataView.Description ?? ""; // kalau null terisi "" --> Empty
            dataModel.CreateBy = IdUser;
            dataModel.CreateDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                respon.Message = "Data Succes Save";
                respon.Entity = dataModel;
            }catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Save : " + ex.Message;
                respon.Entity = dataModel;
            }
            return respon;
        }

        public VMTblVariant GetById(int id)
        {
            VMTblVariant dataView = (from v in db.TblVariants
                                     join c in db.TblCategories on v.IdCategory equals c.Id
                                     where v.IsDelete == false && v.Id == id
                                     select new VMTblVariant
                                     {
                                         Id = v.Id,
                                         NameVariant = v.NameVariant,
                                         Description = v.Description,
                                         NameCategory = c.NameCategory,
                                         IdCategory = v.IdCategory,
                                     }).FirstOrDefault()!;

            return dataView;
        }

        public VMResponse Edit(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id);
            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description ?? "";
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data Succes Saved";
                respon.Entity = dataModel;
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Data Succes Failed = " + ex.Message;
                respon.Entity = dataView;
            }
            return respon;
        }

        public VMResponse Delete(VMTblVariant dataView)
        {

            TblVariant dataModel = db.TblVariants.Find(dataView.Id);
            dataModel.IsDelete = true;           
            dataModel.UpdateBy = IdUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                respon.Message = "Data Succes Deleted";
                respon.Entity = dataModel;
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Data Delete Failed = " + ex.Message;
                respon.Entity = dataView;
            }
            return respon;
        }
    }
}
