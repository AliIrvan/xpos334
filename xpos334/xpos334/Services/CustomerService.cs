﻿using Newtonsoft.Json;
using System.Text;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class CustomerService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public CustomerService(IConfiguration _configuration) 
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMTblCustomer>> GetAllData()
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse);
            return data;
        }
        public async Task<bool> CheckByName(string name, int id, int idRole)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiCustomer/CheckByName/{name}/{id}/{idRole}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }
        public async Task<bool> CheckByEmail(string email, int id)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiCustomer/CheckByEmail/{email}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }
        public async Task<VMResponse> Create(VMTblCustomer dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMTblCustomer> GetDataById(int id)
        {
            VMTblCustomer data = new VMTblCustomer();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse);
            return data;
        }
        public async Task<VMResponse> Edit(VMTblCustomer dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

    }
}
