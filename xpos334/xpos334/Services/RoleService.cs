﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class RoleService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public RoleService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<TblRole>> GetAllData()
        {
            string apirespon = await client.GetStringAsync(RouteAPI + "apiRole/GetAllData");

            List <TblRole> data = JsonConvert.DeserializeObject<List<TblRole>>(apirespon);
            return data;
        }
        public async Task<bool> CheckRoleByName(string roleName, int id)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiRole/CheckRoleByName/{roleName}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> Create(TblRole dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiRole/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<TblRole> GetDataById(int id)
        {
            TblRole data = new TblRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById/{id}"); // client di inisialisasi di atas
            data = JsonConvert.DeserializeObject<TblRole>(apiResponse);
            return data;
        }
        public async Task<VMTblRole> GetDataById_MenuAccess(int id)
        {
            VMTblRole data = new VMTblRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiRole/GetDataById_MenuAccess/{id}"); // client di inisialisasi di atas
            data = JsonConvert.DeserializeObject<VMTblRole>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(TblRole dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Edit_MenuAccess(VMTblRole dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiRole/Edit_MenuAccess", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createdBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiRole/Delete/{id}/{createdBy}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

    }
}
