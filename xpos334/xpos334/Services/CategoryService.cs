﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public CategoryService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<TblCategory>> GetAllData()
        {
            List<TblCategory> data = new List<TblCategory>();
            string apirespon = await client.GetStringAsync(RouteAPI + "apiCategory/GetAllData");
            data = JsonConvert.DeserializeObject<List<TblCategory>>(apirespon);

            return data;
        }

        public async Task<VMResponse> Create(TblCategory dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCategory/Save", content);

            if(request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckCategoryByName(string nameCategory, int id)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{nameCategory}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<TblCategory> GetDataById(int id)
        {
            TblCategory data = new TblCategory();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}" ); // client di inisialisasi di atas
            data = JsonConvert.DeserializeObject<TblCategory>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(TblCategory dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

    }
}
