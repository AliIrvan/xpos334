﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class VariantService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public VariantService(IConfiguration _configuration) 
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMTblVariant>> GetAllData()
        {
            List<VMTblVariant> data = new List<VMTblVariant>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiVariant/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse);
            return data;
        }

        public async Task<VMTblVariant> GetDataById(int id)
        {
            VMTblVariant data = new VMTblVariant();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblVariant>(apiResponse);
            return data;
        }

        public async Task<List<VMTblVariant>> GetDataByIdCategory(int id)
        {
            List<VMTblVariant> data = new List<VMTblVariant>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataByIdCategory/{id}");
            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse);
            return data;
        }

        public async Task<bool> CheckByName(string name, int id, int idCategory)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiVariant/CheckByName/{name}/{id}/{idCategory}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> Create(VMTblVariant dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiVariant/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        public async Task<VMResponse> Edit(VMTblVariant dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiVariant/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiVariant/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> MultipleDelete(List<int> listId)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(listId);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiVariant/MultipleDelete", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

    }
}
