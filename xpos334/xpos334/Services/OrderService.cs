﻿using Newtonsoft.Json;
using System.Text;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class OrderService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public OrderService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<List<VMOrderHeader>> GetDataOrderHeaderDetail(int IdCustomer)
        {
            List<VMOrderHeader> data = new List<VMOrderHeader>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");
            data = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiResponse);
            return data;
        }

        public async Task<int> CountTransaction(int IdCustomer)
        {
            //int data = 0;

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/CountTransaction/{IdCustomer}");
            int data = JsonConvert.DeserializeObject<int>(apiResponse);
            return data;
        }

    }
}
