﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductController : ControllerBase
    {
        private readonly XPOS_334Context db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiProductController(XPOS_334Context _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMTblProduct> GetAllData()
        {
            // INNER JOIN
            List<VMTblProduct> data = ( from p in db.TblProducts
                                        join v in db.TblVariants on p.IdVariant equals v.Id
                                        join c in db.TblCategories on v.IdCategory equals c.Id
                                        where p.IsDelete == false
                                        select new VMTblProduct
                                        {
                                            Id = p.Id,
                                            NameProduct = p.NameProduct,
                                            Price = p.Price,
                                            Stock = p.Stock,
                                            Image = p.Image,

                                            IdVariant = p.IdVariant,
                                            NameVariant = v.NameVariant,

                                            IdCategory = v.IdCategory,
                                            NameCategory = c.NameCategory,

                                            CreateDate = p.CreateDate,
                                        }).ToList();
            return data;
        }

        // LEFT JOIN
        [HttpGet("GetAllData_LeftJoin")]
        public List<VMTblProduct> GetAllData_LeftJoin()
        {
            List<VMTblProduct> data = (from p in db.TblProducts
                                       join v in db.TblVariants on p.IdVariant equals v.Id
                                       join c in db.TblCategories on v.IdCategory equals c.Id into tc from tcategory in tc.DefaultIfEmpty()
                                       where p.IsDelete == false && tcategory == null //--> klo != memunculkan yang ada, artinya jadi innerjoin lagi
                                       select new VMTblProduct
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           Image = p.Image,

                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = tcategory.NameCategory ?? "", // c ganti dengan tcategory karana udah di ganti nama    tetap muncul jika where diatas tidak divalidasi

                                           CreateDate = p.CreateDate,
                                       }).ToList();
            return data;
        }
        [HttpGet("GetDataById/{id}")]
        public VMTblProduct GetDataById(int id)
        {
            // INNER JOIN
            VMTblProduct data = (from p in db.TblProducts
                                       join v in db.TblVariants on p.IdVariant equals v.Id
                                       join c in db.TblCategories on v.IdCategory equals c.Id
                                       where p.IsDelete == false && p.Id == id
                                       select new VMTblProduct
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           Image = p.Image,

                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory,

                                           CreateDate = p.CreateDate,
                                       }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("CheckByName/{name}/{id}/{idVariant}")]
        public bool CheckName(string name, int id, int idVariant)
        {
            TblProduct data = new TblProduct();
            if (id == 0)
            {
                data = db.TblProducts.Where(a => a.NameProduct == name && a.IsDelete == false &&
                                            a.IdVariant == idVariant).FirstOrDefault()!;
            }
            else
            {
                data = db.TblProducts.Where(a => a.NameProduct == name && a.IsDelete == false &&
                                            a.IdVariant == idVariant && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblProduct data)
        {
            //selebihnya dpat dari frontend
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data save Success";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Save Error " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]// tidak pake parameter {}, pake request body karna melempar class
        public VMResponse Edit(TblProduct data)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.NameProduct = data.NameProduct;
                dt.IdVariant = data.IdVariant;
                dt.Price = data.Price;
                dt.Stock = data.Stock;

                // tidak pake ?? "" agar kalo tidak diupdate yang lama tidak hilang
                if(data.Image != null)
                {
                    dt.Image = data.Image;
                }
                
                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Save Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Save Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]// tidak pake parameter {}, pake request body karna melempar class
        public VMResponse Delete(int id)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;

                dt.UpdateBy = IdUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Delete Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Delete Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }


    }
}
