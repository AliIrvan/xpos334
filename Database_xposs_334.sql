CREATE DATABASE XPOS_334

CREATE TABLE TblCategory(
	Id INT PRIMARY KEY IDENTITY(1,1),
	NameCategory VARCHAR(50) NOT NULL,
	Description VARCHAR(100),
	IsDelete BIT,
	CreateBy INT NOT NULL,
	CreateDate DATETIME NOT NULL,
	UpdateBy INT,
	UpdateDate DATETIME
)

INSERT INTO TblCategory(NameCategory, Description, IsDelete, CreateBy, CreateDate)
VALUES
	('Drink','Desc of Drink', 0, 1, GETDATE()),
	('Dessert','Desc of Dessert', 0, 1, GETDATE())
	--('TEST','desc of test',0,1,GETDATE()),
	--('TEST2','desc of test',0,1,GETDATE()),
	--('TEST3','desc of test',0,1,GETDATE()),
	--('TEST4','desc of test',0,1,GETDATE()),
	--('TEST5','desc of test',0,1,GETDATE())
CREATE TABLE TblVariant(
	Id INT PRIMARY KEY IDENTITY(1,1),
	IdCategory INT NOT NULL,
	NameVariant VARCHAR(50) NOT NULL,
	Description VARCHAR(MAX),
	IsDelete BIT,
	CreateBy INT NOT NULL,
	CreateDate DATETIME NOT NULL,
	UpdateBy INT,
	UpdateDate DATETIME
)

SELECT * FROM TblVariant
SELECT * FROM TblCategory
SElECT * FROM TblProduct

INSERT INTO TblVariant(IdCategory, NameVariant, Description, IsDelete, CreateBy, CreateDate, UpdateBy, UpdateDate)
VALUES
	(1, 'Le Minerale','Desc of Le Minerale', 0, 1, GETDATE(), NULL , NULL),
	(1, 'Le Minerale 2','Desc of Le Minerale 2', 0, 1, GETDATE(), NULL, NULL),
	(2, 'Cake','Desc of Le Cake', 0, 1, GETDATE(), NULL, NULL),
	(2, 'Cake 2','Desc of Le Cake 2', 0, 1, GETDATE(), NULL, NULL)


CREATE TABLE TblProduct(
	Id INT PRIMARY KEY IDENTITY(1,1),
	IdVariant INT NOT NULL,
	NameProduct VARCHAR(100) NOT NULL,
	Price DECIMAL(18,0) NOT NULL,
	Stock INT NOT NULL,
	[Image] VARCHAR(MAX),
	IsDelete BIT,
	CreateBy INT NOT NULL,
	CreateDate DATETIME NOT NULL,
	UpdateBy INT,
	UpdateDate DATETIME
)

SELECT * FROM TblProduct

CREATE TABLE TblOrderHeader(
	[Id] INT PRIMARY KEY IDENTITY(1,1),
	CodeTransaction NVARCHAR(20) NOT NULL,
	IdCustomer INT NOT NULL,
	Amount DECIMAL(18,2) NOT NULL,
	TotalQty INT NOT NULL,
	IsCheckout BIT NOT NULL,
	IsDelete BIT NULL,
	CreateBy INT NOT NULL,
	CreateDate DATETIME NOT NULL,
	UpdateBy INT NULL,
	UpdateDate DATETIME NULL
)

SELECT * FROM TblOrderHeader

CREATE TABLE TblOrderDetail(
	Id INT PRIMARY KEY IDENTITY(1,1),
	IdHeader INT NOT NULL,
	IdProduct INT NOT NULL,
	Qty INT NOT NULL,
	SumPrice DECIMAL(18,2) NOT NULL,
	IsDelete BIT NULL,
	CreateBy INT NOT NULL,
	CreateDate DATETIME NOT NULL,
	UpdateBy INT NULL,
	UpdateDate DATETIME NULL
)

SELECT * FROM TblOrderDetail


CREATE TABLE TblRole(
	Id INT PRIMARY KEY IDENTITY(1,1),
	RoleName VARCHAR(80) NULL,
	IsDelete BIT NULL,
	CreatedBy INT NOT NULL,
	CreatedDate DATETIME NOT NULL,
	UpdatedBy INT NULL,
	UpdatedDate DATETIME NULL
)

CREATE TABLE TblCustomer(
	Id INT PRIMARY KEY IDENTITY(1,1),
	NameCustomer NVARCHAR(50) NOT NULL,
	Email NVARCHAR(50) NOT NULL,
	[Password] NVARCHAR(100) NOT NULL,
	[Address] NVARCHAR(MAX) NOT NULL,
	Phone NVARCHAR(15) NOT NULL,
	IdRole INT NULL,
	IsDelete BIT NOT NULL,
	CreatedBy INT NOT NULL,
	CreatedDate DATETIME NOT NULL,
	UpdateBy INT NULL,
	UpdateDate DATETIME NULL,
)

create table [dbo].[TblMenu]
(
Id int primary key identity (1,1) not null,
MenuName varchar (80) not null,
MenuAction varchar (80) not null,
MenuController varchar (80) not null,
MenuIcon varchar (80) null,
MenuSorting int null,
Isparent bit null,
MenuParent int null,
IsDelete bit null,
CreatedBy int not null,
CreatedDate datetime not null,
UpdatedBy int null,
UpdatedDate datetime null
)

create table [dbo].[TblMenuAccess]
(
Id int primary key identity (1,1) not null,
RoleId int null,
MenuId int null,
MenuParent int null,
IsDelete bit null,
CreatedBy int not null,
CreatedDate datetime not null,
UpdatedBy int null,
UpdatedDate datetime null
)

SELECT * FROM TblCustomer WHERE IsDelete = 0
SELECT * FROM TblCustomer WHERE IdRole = 2
SELECT * FROM TblMenuAccess WHERE RoleId = 4

UPDATE TblCustomer SET IdRole = 2 WHERE Id = 7


SELECT * from TblMenu AS parent
join TblMenuAccess AS ma
on parent.Id = ma.MenuId 
 where parent.Isparent = 1 AND ma.RoleId = 5 AND parent.IsDelete = 0 AND ma.IsDelete = 0

SELECT * FROM TblRole