﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using System.Data;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiDaftarController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 0;
        public apiDaftarController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("CheckByEmail/{email}")]
        public bool CheckByEmail(string email)
        {
            MUser data = new MUser();
            data = db.MUsers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault();

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("SaveToken")]
        public VMResponse SaveToken(TToken dataParam)
        {
            List<TToken> dataLama = db.TTokens.Where(a => a.Email == dataParam.Email && a.UsedFor == dataParam.UsedFor && a.IsExpired == false).ToList();

            foreach (var item in dataLama)
            {
                item.IsExpired = true;
                db.Update(item);
            }

            db.SaveChanges();

            TToken data = new TToken();
            data.Email = dataParam.Email;
            data.UserId = IdUser;
            data.Token = dataParam.Token;
            data.ExpiredOn = DateTime.Now.AddMinutes(10);
            data.UsedFor = dataParam.UsedFor;
            data.IsExpired = false;

            data.IsDelete = false;
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;


            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("CheckIsExpired/{email}/{token}")]
        public VMResponse CheckIsExpired(string email, string token)
        {
            TToken data = db.TTokens.Where(a => a.Email == email && a.Token == token).FirstOrDefault();
            VMResponse respon = new VMResponse();

            if (data != null) // Kalau ada
            {
                DateTime TimeNow = DateTime.Now;
                string expired = data.ExpiredOn.ToString();
                DateTime ExpiredOn = DateTime.Parse(expired);
                TimeSpan timeSpan = ExpiredOn - TimeNow;

                //if (DateTime.Now < expiredOn)

                if (timeSpan.Minutes < 0)
                {
                    data.IsExpired = true;
                }

                db.Update(data);
                db.SaveChanges();

                TToken checkData = db.TTokens.Where(a => a.Email == email && a.Token == token).FirstOrDefault();

                if (checkData.IsExpired == true)
                {
                    respon.Success = true;
                    respon.Message = "Kode Token telah kadaluarsa, silahkan kirim ulang kode token";
                    return respon;
                }
                else
                {
                    respon.Success = false;
                    respon.Message = "Konfimasi Kode OTP Sukses, silahkan lanjut ke proses selanjutnya";
                    return respon;
                }

            }
            else
            {
                respon.Success = true;
                respon.Message = "Kode tidak tersedia, harap cek ulang kode atau kirim ulang kode";
                return respon;
            }
        }

        [HttpGet("CheckTokenByEmail/{email}/{token}")]
        public bool CheckTokenByEmail(string email, string token)
        {


            TToken data = new TToken();


            data = db.TTokens.Where(a => a.Email == email && a.Token == token && a.IsDelete == false && a.IsExpired == false).FirstOrDefault()!;

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpGet("GetAllDataRole")]
        public List<MRole> GetAllDataRole()
        {
            List<MRole> data = db.MRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpPost("SaveCreateAccount")]
        public VMResponse SaveCreateAccount(VMMUser dataParam)
        {
            MBiodatum dataBio = new MBiodatum();
            dataBio.Fullname = dataParam.FullName;
            dataBio.MobilePhone = dataParam.MobilePhone;
            dataBio.CreatedBy = IdUser;
            dataBio.CreatedOn = DateTime.Now;
            dataBio.IsDelete = false;

            try
            {
                db.Add(dataBio);
                db.SaveChanges();

                MUser dataUser = new MUser();
                dataUser.BiodataId = dataBio.Id;
                dataUser.RoleId = dataParam.RoleId;
                dataUser.Email = dataParam.Email;
                dataUser.Password = dataParam.Password;
                dataUser.CreatedBy = dataBio.CreatedBy;
                dataUser.CreatedOn = DateTime.Now;
                dataUser.IsDelete = false;

                db.Add(dataUser);
                db.SaveChanges();

                TToken dataToken = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsExpired == false).FirstOrDefault();

                if (dataToken != null)
                {
                    dataToken.IsExpired = true;
                    dataToken.UserId = dataUser.Id;

                    db.Update(dataToken);

                }

                dataBio.CreatedBy = dataUser.Id;
                db.Update(dataBio);

                dataUser.CreatedBy = dataUser.Id;
                db.Update(dataUser);

                db.SaveChanges();

                // Penambahan data ke table admin, pasien, dokter sesuai inputan role
                if (dataParam.RoleId == 1) // Role Admin
                {
                    MAdmin dataAdmin = new MAdmin();
                    dataAdmin.BiodataId = dataBio.Id;
                    dataAdmin.Code = dataBio.Id.ToString() + "-" + dataBio.CreatedOn.ToString("ddMMyy");
                    dataAdmin.CreatedBy = dataUser.Id;
                    dataAdmin.CreatedOn = DateTime.Now;
                    dataAdmin.IsDelete = false;

                    db.Add(dataAdmin);
                    db.SaveChanges();
                }
                else if (dataParam.RoleId == 2) // Role Pasien/Customer
                {
                    MCustomer dataCustomer = new MCustomer();
                    dataCustomer.BiodataId = dataBio.Id;
                    dataCustomer.CreatedBy = dataUser.Id;
                    dataCustomer.CreatedOn = DateTime.Now;
                    dataCustomer.IsDelete = false;

                    db.Add(dataCustomer); 
                    db.SaveChanges();
                }
                else if(dataParam.RoleId == 3) // Role Dokter
                {
                    MDoctor dataDoctor = new MDoctor();
                    dataDoctor.BiodataId = dataBio.Id;
                    dataDoctor.CreatedBy = dataUser.Id;
                    dataDoctor.CreatedOn = DateTime.Now;
                    dataDoctor.IsDelete = false;

                    db.Add(dataDoctor);
                    db.SaveChanges();
                }

                respon.Message = "Data Success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPost("SaveNewPassword")]
        public VMResponse SaveNewPassword(VMMUser dataParam)
        {
            MUser oldData = db.MUsers.Where(a => a.Email == dataParam.Email && a.IsDelete == false).FirstOrDefault();

            var oldPassword = oldData.Password;
            oldData.Password = dataParam.Password;
            oldData.ModifiedBy = oldData.Id;
            oldData.ModifiedOn = DateTime.Now;
            oldData.IsDelete = false;

            try
            {
                db.Update(oldData);
                db.SaveChanges();

                TResetPassword newData = new TResetPassword();
                newData.OldPassword = oldPassword;
                newData.NewPassword = dataParam.Password;
                newData.CreatedBy = oldData.Id;
                newData.CreatedOn = oldData.CreatedOn;
                newData.ModifiedBy = oldData.Id;
                newData.ModifiedOn = DateTime.Now;
                newData.IsDelete = false;

                db.Add(newData);

                TToken dataToken = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsExpired == false).FirstOrDefault();

                if (dataToken != null)
                {
                    dataToken.IsExpired = true;
                    db.Update(dataToken);

                }

                db.SaveChanges();

                respon.Message = "Ganti password berhasil!";


            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Ganti Password gagal : " + ex.Message;
            }

            return respon;

        }
    }
}
