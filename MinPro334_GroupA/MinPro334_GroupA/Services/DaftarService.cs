﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using NuGet.Common;
using System.Text;
using System.Xml.Linq;

namespace MinPro334_GroupA.Services
{
    public class DaftarService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public DaftarService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<bool> CheckByEmail(string email)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiDaftar/CheckByEmail/{email}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> GenerateToken(VMTToken dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiDaftar/SaveToken", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> CheckIsExpired(string email, string token)
        {
            var apiRespon = await client.GetStringAsync(RouteAPI + $"apiDaftar/CheckIsExpired/{email}/{token}");
            VMResponse respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            return respon;
        }

        public async Task<bool> CheckTokenByEmail(string email, string token)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDaftar/CheckTokenByEmail/{email}/{token}");
            bool isvalid = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isvalid;
        }

        public async Task<VMResponse> SetNewPassword(VMMUser dataParam) 
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiDaftar/SaveNewPassword", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<List<MRole>> GetAllDataRole()
        {
            List<MRole> data = new List<MRole>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDaftar/GetAllDataRole");
            data = JsonConvert.DeserializeObject<List<MRole>>(apiResponse);
            return data;
        }

        public async Task<VMResponse> CreateAccount(VMMUser dataParam) 
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiDaftar/SaveCreateAccount", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


    }
}
