﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using NuGet.Common;
using System.Net;
using System.Net.Mail;

namespace MinPro334_GroupA.Controllers
{
    public class DaftarController : Controller
    {
        private DaftarService daftarService;
        private int IdUser = 1;

        public DaftarController(DaftarService _daftarService)
        {
            daftarService = _daftarService;
        }
        public IActionResult Register()   
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckEmailIsExist(string email)
        {
            bool isExist = await daftarService.CheckByEmail(email);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateOTP(VMTToken dataParam)
        {
            var kodeOTP = CreateOTP();   

            //Send Kode OTP via Email
            var fromMail = new MailAddress("mydokterpocket@gmail.com", "Pockey");
            var fromEmailpassword = "eatn wexl elhy tubp";
            var toMail = new MailAddress(dataParam.Email);

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Initial Login";
            Message.Body = "<br/> OTP : " + kodeOTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);

            dataParam.Token = kodeOTP;

            
            VMResponse respon = await daftarService.GenerateToken(dataParam);
          
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }

        public string CreateOTP()
        {
            string OTPLength = "6";
            string OTP = string.Empty;

            string Chars = string.Empty;
            Chars = "1,2,3,4,5,6,7,8,9,0";

            char[] seplitChar = { ',' };
            string[] arr = Chars.Split(seplitChar);
            string NewOTP = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < Convert.ToInt32(OTPLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                NewOTP += temp;
                OTP = NewOTP;
            }
            return OTP;
        }

        public async Task<IActionResult> KonfirmasiOTP(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }
        public async Task<IActionResult> KonfirmasiOTPLupaPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        public async Task<JsonResult> CheckTokenIsExpired(string email, string token) 
        {
            VMResponse respon = await daftarService.CheckIsExpired(email, token);

            //bool isvalid = await daftarService.CheckIsExpired(email, token);
            //return Json(isvalid);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return Json(new { dataRespon = respon });
        }

        public async Task<JsonResult> CheckTokenByEmail(string email, string token)
        {
            bool isvalid = await daftarService.CheckTokenByEmail(email, token);
            return Json(isvalid);
        }
        public async Task<IActionResult> SetPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        public async Task<IActionResult> SetPasswordLupaPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> SetPasswordLupaPassword(VMMUser dataParam) 
        {
            dataParam.Id = dataParam.Id == null ? IdUser : dataParam.Id;
            dataParam.FullName = dataParam.FullName ?? "";
            dataParam.MobilePhone = dataParam.MobilePhone ?? "";
            dataParam.CreatedBy = dataParam.CreatedBy == null ? 0 : dataParam.CreatedBy;
            dataParam.CreatedOn = dataParam.CreatedOn == null ? DateTime.Now : dataParam.CreatedOn;
            dataParam.IsDelete = dataParam.IsDelete == null ? false : dataParam.IsDelete;
            
            VMResponse respon = await daftarService.SetNewPassword(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }
        public async Task<IActionResult> SignUp(string email, string password) 
        {
            ViewBag.Email = email;
            ViewBag.Password = password;

            List<MRole> listRole = await daftarService.GetAllDataRole();
            ViewBag.listRole = listRole;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAccount(VMMUser dataParam)
        {
            //dataParam.MobilePhone = dataParam.MobilePhone ?? "";
            //dataParam.RoleId = dataParam.RoleId ?? 0;
            VMResponse respon = await daftarService.CreateAccount(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public IActionResult LupaPassword()
        {
            return PartialView();
        }




    }
}
